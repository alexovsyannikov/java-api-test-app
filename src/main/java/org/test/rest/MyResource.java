package org.test.rest;

import com.atlassian.jira.component.ComponentAccessor;
import com.stiltsoft.jira.attachcategory.facade.SmartAttachmentsFacade;
import com.stiltsoft.jira.attachcategory.facade.entity.config.Scheme;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import java.util.List;

import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

@Path("test")
public class MyResource {

    private final SmartAttachmentsFacade facade;

    public MyResource() {
        this.facade = ComponentAccessor.getOSGiComponentInstanceOfType(SmartAttachmentsFacade.class);
    }

    @GET
    @Produces({TEXT_PLAIN})
    public Response test() {

        List<Scheme> schemes = facade.getSchemes();

        return Response.ok(Integer.toString(schemes.size())).build();
    }
}
