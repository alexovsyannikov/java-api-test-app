package org.test.workflow.validator;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.opensymphony.workflow.spi.WorkflowEntry;
import com.stiltsoft.jira.attachcategory.facade.SmartAttachmentsFacade;
import com.stiltsoft.jira.attachcategory.facade.entity.issue.AttachmentCategories;
import com.stiltsoft.jira.attachcategory.facade.entity.issue.AttachmentCategory;
import org.ofbiz.core.entity.GenericEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class CustomValidator implements Validator {

    private static final Logger log = LoggerFactory.getLogger(CustomValidator.class);
    private final SmartAttachmentsFacade facade;

    public CustomValidator(@ComponentImport SmartAttachmentsFacade facade) {
        this.facade = facade;
    }

    @Override
    public void validate(Map transientVars, Map args, PropertySet propertySet) throws WorkflowException {
        Issue issue = getIssue(transientVars);

        try {
            AttachmentCategories categories = facade.getAttachmentCategories(issue);

            List<AttachmentCategory> categoryList = categories.getCategories();

            Optional<AttachmentCategory> categoryDocumentsOptional = categoryList.stream()
                    .filter(attachmentCategory -> attachmentCategory.getName().equals("Documents"))
                    .findFirst();

            if (categoryDocumentsOptional.isPresent()) {
                AttachmentCategory categoryDocuments = categoryDocumentsOptional.get();

                List<Attachment> attachments = categoryDocuments.getAttachments(true);

                long countOfPdfFiles = attachments.stream()
                        .filter(attachment -> attachment.getFilename().contains(".pdf"))
                        .count();

                if (countOfPdfFiles == 0) {
                    throw new InvalidInputException("The issue doesn't contain any PDF file");
                }
            }
        } catch (Throwable throwable) {
            log.error("Block failed", throwable);
            throw new InvalidInputException("Block failed");
        }
    }

    private Issue getIssue(Map transientVars) throws DataAccessException {
        Object issue = transientVars.get("originalissueobject");
        if (issue == null) {
            WorkflowEntry entry = (WorkflowEntry) transientVars.get("entry");

            try {
                issue = ComponentAccessor.getIssueManager().getIssueObjectByWorkflow(entry.getId());
            } catch (GenericEntityException var5) {
                throw new DataAccessException("entry-issue");
            }

            if (transientVars.containsKey("issue")) {
                return (Issue) transientVars.get("issue");
            }

            if (issue == null) {
                throw new DataAccessException("no-issue-found");
            }
        }

        return (Issue) issue;
    }
}
