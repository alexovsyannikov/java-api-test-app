package org.test.workflow.validator;

import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;

import java.util.HashMap;
import java.util.Map;

public class CustomValidatorFactory implements WorkflowPluginValidatorFactory {

    @Override
    public Map<String, ?> getVelocityParams(String s, AbstractDescriptor abstractDescriptor) {
        return new HashMap<>();
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> map) {
        return map;
    }
}
