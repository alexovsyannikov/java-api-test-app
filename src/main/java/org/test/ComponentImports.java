package org.test;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.stiltsoft.jira.attachcategory.facade.SmartAttachmentsFacade;

public class ComponentImports {

    @ComponentImport
    SmartAttachmentsFacade facade;
}
